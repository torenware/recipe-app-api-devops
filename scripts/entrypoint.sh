#! /bin/sh

# Exit on error
set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

# The following can be tuned; for our simple REST API 4 workers in the
# container is fairly efficient. the module arg points to app/wsgi.py.
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi

