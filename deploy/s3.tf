resource "aws_s3_bucket" "app_public_files" {

  # bucket_prefix will append random characters
  # to guarantee the name is unique.
  bucket_prefix = "${local.prefix}-files"
  acl           = "public-read"

  # Dangerous setting, obviously:
  force_destroy = true
}

