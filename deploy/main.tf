terraform {
  # Define a backend, needed to use CI/CD
  backend "s3" {
    bucket         = "rmtoren-recipe-api-tutorial-state"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"

  }
}

provider "aws" {
  region = "us-east-1"
  # When you need to update, check the terraform and aws sites to
  # make sure you're using a sensible version.
  version = "~> 2.54.0"
}

# locals are variables.
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# We need to declare this for network.tf
data "aws_region" "current" {}


