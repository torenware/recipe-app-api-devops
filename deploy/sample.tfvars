# Sample terraform.tfvars
#
# used for local development. The terraform.tfvars file
# is (or should be) in .gitignore.  Staging and production
# get their vars from gitlab's internal CI variables.
#
# In gitlab, pass my_variable to terraform as TF_VAR_my_variable.
#
db_username       = "recipeapp"
db_password       = "changeme"
django_secret_key = "change me"
