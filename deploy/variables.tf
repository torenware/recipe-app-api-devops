
variable "prefix" {
  # We want to name our AWS resources. To help keep them
  # straight, we'll add a prefix to the names. Used a lot,
  # so best to keep short. We use "raad" as short for
  # "recipe-app-api-devops".
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "engineering@my-email.com"
}

variable "db_username" {
  description = "Username for RDS Postgresql"
}

variable "db_password" {
  description = "User password for RDS Postgresql"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "802129099889.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "802129099889.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "django_debug" {
  description = "Django debug mode flag (0/1)"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "torensys.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}




